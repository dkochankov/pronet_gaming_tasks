package pronet.gaming.tasks.pronet.gaming.tasks.repositories;

import pronet.gaming.tasks.pronet.gaming.tasks.models.InputData;

public interface DataStorageRepository {
    void writeRecordToDb(InputData inputData);
    void loadCsv(String filePath);
}
