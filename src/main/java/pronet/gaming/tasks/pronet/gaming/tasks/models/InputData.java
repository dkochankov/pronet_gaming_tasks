package pronet.gaming.tasks.pronet.gaming.tasks.models;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Comparator;

@Entity
@Table(name = "INPUT_DATA")
public class InputData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;


        //@Id
        @Column(name="MATCH_ID")
        private String matchId;

        @Column(name = "MARKET_ID")
        private String marketId;

        @Column(name = "OUTCOME_ID")
        private String outcomeId;

        @Column(name = "SPECIFIERS")
        private String specifiers;

        @Column(name = "DATE_INSERT")
        private Timestamp insertionDate;

    public InputData() { }

    public InputData(String matchId, String marketId, String outcomeId, String specifiers) {
        this.matchId = matchId;
        this.marketId = marketId;
        this.outcomeId = outcomeId;
        this.specifiers = specifiers;
    }

    public InputData(String matchId, String marketId, String outcomeId, String specifiers, Timestamp insertionDate) {
        this.matchId = matchId;
        this.marketId = marketId;
        this.outcomeId = outcomeId;
        this.specifiers = specifiers;
        this.insertionDate = insertionDate;
    }

    public String getMatchId() {
        return matchId;
    }

    public void setMatchId(String matchId) {
        this.matchId = matchId;
    }

    public String getMarketId() {
        return marketId;
    }

    public void setMarketId(String marketId) {
        this.marketId = marketId;
    }

    public String getOutcomeId() {
        return outcomeId;
    }

    public void setOutcomeId(String outcomeId) {
        this.outcomeId = outcomeId;
    }

    public String getSpecifiers() {
        return specifiers;
    }

    public void setSpecifiers(String specifiers) {
        this.specifiers = specifiers;
    }

    public Timestamp getInsertionDate() {
        return insertionDate;
    }

    public void setInsertionDate(Timestamp insertionDate) {
        this.insertionDate = insertionDate;
    }

    public static Comparator<InputData> InputDataComparator = new Comparator<InputData>(){

        public int compare(InputData d1, InputData d2)
        {
            int result = d1.getMatchId().compareTo(d2.getMatchId());
            if (result != 0)
            {
                return result;
            }
            result = d1.getMarketId().compareTo(d2.getMarketId());
            if (result != 0)
            {
                return result;
            }
            result = d1.getOutcomeId().compareTo(d2.getOutcomeId());
            if (result != 0)
            {
                return result;
            }
            result = d1.getSpecifiers().compareTo(d2.getSpecifiers());
            if (result != 0)
            {
                return result;
            }
            return d1.getInsertionDate().compareTo(d2.getInsertionDate());
        }};

}
