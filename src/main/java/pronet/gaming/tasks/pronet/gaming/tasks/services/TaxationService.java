package pronet.gaming.tasks.pronet.gaming.tasks.services;

//import net.minidev.json.JSONObject;

import com.fasterxml.jackson.databind.node.ObjectNode;

public interface TaxationService {
    ObjectNode getGeneralReturnAmounts(ObjectNode obj);
    ObjectNode getWinningsReturnAmounts(ObjectNode obj);
}
