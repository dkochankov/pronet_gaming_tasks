package pronet.gaming.tasks.pronet.gaming.tasks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PronetGamingTasksApplication {

	public static void main(String[] args) {
		SpringApplication.run(PronetGamingTasksApplication.class, args);
	}

}
