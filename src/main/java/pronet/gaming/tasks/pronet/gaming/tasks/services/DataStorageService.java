package pronet.gaming.tasks.pronet.gaming.tasks.services;

import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

public interface DataStorageService {
    ObjectNode storeFileData(MultipartFile file, HttpServletRequest request) throws Exception;
}
