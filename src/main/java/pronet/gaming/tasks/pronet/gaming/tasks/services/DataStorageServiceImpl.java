package pronet.gaming.tasks.pronet.gaming.tasks.services;

import java.io.File;
import java.io.FileWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import pronet.gaming.tasks.pronet.gaming.tasks.models.InputData;
import pronet.gaming.tasks.pronet.gaming.tasks.repositories.DataStorageRepository;
import javax.servlet.http.HttpServletRequest;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class DataStorageServiceImpl implements DataStorageService {

    private final DataStorageRepository dataStorageRepository;

    @Autowired
    public DataStorageServiceImpl(DataStorageRepository dataStorageRepository) {
        this.dataStorageRepository = dataStorageRepository;
    }


    public ObjectNode storeFileData(MultipartFile file, HttpServletRequest request) throws Exception {

        File dest;
        try {
            String realPathtoUploads = getUploadFilePath(request);
            if (!new File(realPathtoUploads).exists()) {
                new File(realPathtoUploads).mkdir();
            }
            String orgName = file.getOriginalFilename();
            String filePath = realPathtoUploads + orgName;
            System.out.println(filePath);
            dest = new File(filePath);
            file.transferTo(dest);
        } catch(Exception ex){
            throw new Exception();
        }


        List<InputData> s = new ArrayList<>();

        try (Stream<String> lines = Files.lines (dest.toPath(), StandardCharsets.UTF_8))
        {
            for (String line : (Iterable<String>) lines.skip(1)::iterator)
            {
                InputData inputData = null;
                String[] splitted = Arrays.stream(line.split("\\|")).map(String::trim).toArray(String[]::new);
                if(splitted.length==1){
                    inputData = new InputData(splitted[0], null, null, null, createTimestamp());
                } else if(splitted.length==2){
                    inputData = new InputData(splitted[0], splitted[1], null, null, createTimestamp());
                } else if(splitted.length==3){
                    inputData = new InputData(splitted[0], splitted[1], splitted[2], null, createTimestamp());
                } else if(splitted.length>3){
                    String specifiers = String.join(" ", Arrays.copyOfRange(splitted, 3, splitted.length));
                    inputData = new InputData(splitted[0], splitted[1], splitted[2], specifiers, createTimestamp());
                }
                s.add(inputData);
            }
        }

        List<InputData> res = s.parallelStream().sorted(InputData.InputDataComparator).collect(Collectors.toList());
        String filePath = writeToCsv(res, request);
        dataStorageRepository.loadCsv(filePath);

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode result = mapper.createObjectNode();
        String minTime = s.stream().min(Comparator.comparing(InputData::getInsertionDate)).get().getInsertionDate().toString();
        String maxTime = s.stream().max(Comparator.comparing(InputData::getInsertionDate)).get().getInsertionDate().toString();
        result.put("minInsertionTime", minTime);
        result.put("maxInsertionTime", maxTime);
        return result;
    }

    private void displayObjProperties(InputData inputData){
        ObjectMapper mapper = new ObjectMapper();
        try{
            System.out.println(mapper.writeValueAsString(inputData));
        } catch(Exception e){
            System.out.println(e.getMessage());
        }
    }

    private String writeToCsv(List<InputData> s, HttpServletRequest request) {
        final String FILE_HEADER ="MATCH_ID, MARKET_ID, OUTCOME_ID, SPECIFIERS, DATE_INSERT";
        final String COMMA_DELIMITER = ",";
        final String NEW_LINE_SEPARATOR = "\n";
        String realPathtoUploads = getUploadFilePath(request);
        String fileName = "output.csv";
        String fullFilePath = realPathtoUploads + fileName;
        try {
            FileWriter fw = new FileWriter(fullFilePath);
            fw.append(FILE_HEADER);
            fw.append(NEW_LINE_SEPARATOR);
            for(InputData i : s){
                try {
                    displayObjProperties(i);
                    fw.append(i.getMatchId());
                    fw.append(COMMA_DELIMITER);
                    fw.append(i.getMarketId());
                    fw.append(COMMA_DELIMITER);
                    fw.append(i.getOutcomeId());
                    fw.append(COMMA_DELIMITER);
                    fw.append(i.getSpecifiers());
                    fw.append(COMMA_DELIMITER);
                    fw.append(i.getInsertionDate().toLocalDateTime().toString());
                    fw.append(NEW_LINE_SEPARATOR);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            fw.flush();
            fw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fullFilePath;
    }


    private String getUploadFilePath(HttpServletRequest request){
        String uploadsDir = "/uploads/";
        return request.getServletContext().getRealPath(uploadsDir);
    }

    private Timestamp createTimestamp(){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        return Timestamp.valueOf(now);
    }

}
