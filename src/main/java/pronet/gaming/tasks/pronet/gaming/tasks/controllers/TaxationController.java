package pronet.gaming.tasks.pronet.gaming.tasks.controllers;

//import org.json.JSONObject;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pronet.gaming.tasks.pronet.gaming.tasks.services.TaxationService;

@RestController
public class TaxationController {

    private final TaxationService taxationService;

    @Autowired
    public TaxationController(TaxationService taxationService) {
        this.taxationService = taxationService;
    }

    @PostMapping(path = "/generalReturnAmounts", produces= MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ObjectNode> getGeneralReturnAmounts (@RequestBody ObjectNode obj)
    {
        return new ResponseEntity<>(taxationService.getGeneralReturnAmounts(obj), HttpStatus.OK); }

    @PostMapping(path = "/winningsReturnAmounts", produces= MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ObjectNode> getWinningsReturnAmounts (@RequestBody ObjectNode obj)
    {
        return new ResponseEntity<>(taxationService.getWinningsReturnAmounts(obj), HttpStatus.OK); }


}
