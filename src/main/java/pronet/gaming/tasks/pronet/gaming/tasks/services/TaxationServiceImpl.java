package pronet.gaming.tasks.pronet.gaming.tasks.services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.stereotype.Service;

@Service
public class TaxationServiceImpl implements TaxationService {
    public static final double TAX_RATE = 0.1;

    public ObjectNode getGeneralReturnAmounts(ObjectNode obj){
        double possibleReturnAmountBefTax = calculatePossibleReturnAmountBefTax(obj);
        double possibleReturnAmountAfterTaxRate = possibleReturnAmountBefTax - (possibleReturnAmountBefTax * 0.1);
        double possibleReturnAmountAfterTaxAmount = possibleReturnAmountBefTax -2;
        double taxAmount = 2;
        return createJSONresponse(possibleReturnAmountBefTax, possibleReturnAmountAfterTaxRate,
                possibleReturnAmountAfterTaxAmount, TAX_RATE, taxAmount);
    }

    public ObjectNode getWinningsReturnAmounts(ObjectNode obj){
        double playedAmount = obj.get("incoming").get("playedAmount").asDouble();
        double possibleReturnAmountBefTax = calculatePossibleReturnAmountBefTax(obj);
        double possibleReturnAmountAfterTaxRate = possibleReturnAmountBefTax - (possibleReturnAmountBefTax - playedAmount) * 0.1;
        double possibleReturnAmountAfterTaxAmount = possibleReturnAmountBefTax - (possibleReturnAmountBefTax - playedAmount -1);
        double taxAmount = 1;
        return createJSONresponse(possibleReturnAmountBefTax, possibleReturnAmountAfterTaxRate,
                possibleReturnAmountAfterTaxAmount, TAX_RATE, taxAmount);
    }


    private ObjectNode createJSONresponse(double possibleReturnAmountBefTax, double possibleReturnAmountAfterTaxRate,
                                          double possibleReturnAmountAfterTaxAmount, double taxRate, double taxAmount){

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode result = mapper.createObjectNode();
        ObjectNode amounts = mapper.createObjectNode();
        amounts.put("possibleReturnAmountBefTax", possibleReturnAmountBefTax);
        amounts.put("possibleReturnAmountAfterTaxRate", possibleReturnAmountAfterTaxRate);
        amounts.put("possibleReturnAmountAfterTaxAmount", possibleReturnAmountAfterTaxAmount);
        amounts.put("taxRate", taxRate);
        amounts.put("taxAmount", taxAmount);
        result.set("outgoing", amounts);
        return result;
    }

    private double calculatePossibleReturnAmountBefTax(ObjectNode obj){
        JsonNode inputData = obj.get("incoming");
        double playedAmount = inputData.get("playedAmount").asDouble();
        double odd = inputData.get("odd").asDouble();
        return playedAmount * odd;
    }

}
