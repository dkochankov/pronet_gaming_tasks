package pronet.gaming.tasks.pronet.gaming.tasks.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pronet.gaming.tasks.pronet.gaming.tasks.services.DataStorageService;
import javax.servlet.http.HttpServletRequest;

@RestController
public class DataStorageController {

    private final DataStorageService dataStorageService;

    @Autowired
    public DataStorageController(DataStorageService dataStorageService) {
        this.dataStorageService = dataStorageService;
    }

    @Autowired
    private HttpServletRequest request;

    @PostMapping(path = "/fileDataStorage")
    public ResponseEntity<ObjectNode> getGeneralReturnAmounts (@RequestParam("file") MultipartFile file, HttpServletRequest request) {
        try {
            return new ResponseEntity<>(dataStorageService.storeFileData(file, request), HttpStatus.OK);
        } catch (Exception ex) {
            ObjectMapper mapper = new ObjectMapper();
            ObjectNode result = mapper.createObjectNode();
            result.put("error", "error storing data" + ex.getMessage());
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
    }
}
