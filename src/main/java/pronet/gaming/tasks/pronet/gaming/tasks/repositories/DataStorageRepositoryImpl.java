package pronet.gaming.tasks.pronet.gaming.tasks.repositories;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pronet.gaming.tasks.pronet.gaming.tasks.models.InputData;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class DataStorageRepositoryImpl implements DataStorageRepository {

    @PersistenceContext
    EntityManager entityManager;

    private final SessionFactory sessionFactory;

    @Autowired
    public DataStorageRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void writeRecordToDb(InputData inputData){
        displayObjProperties(inputData);
        Session session = setupSession();
        Transaction transaction = session.beginTransaction();
        transaction.commit();
    }

    private Session setupSession() {
        Session session;
        try {
            session = sessionFactory.getCurrentSession();
        } catch (Exception e) {
            session = sessionFactory.openSession();
        }
        return session;
    }

    private void displayObjProperties(InputData inputData){
        ObjectMapper mapper = new ObjectMapper();
        try{
            System.out.println(mapper.writeValueAsString(inputData));
        } catch(Exception e){
            System.out.println(e.getMessage());
        }
    }

    //to use it change global variable on server ->  SET GLOBAL local_infile=1;
    @Override
    @Transactional
    public void loadCsv(String filePath) {
        String resString = filePath.replace("\\", "/");
        StringBuilder builder = new StringBuilder("LOAD DATA LOCAL INFILE ");
        builder.append("'"+resString+"'");
        builder.append(" INTO TABLE INPUT_DATA FIELDS TERMINATED BY ',' ");
        builder.append(" LINES TERMINATED BY '\\n' IGNORE 1 LINES");

        try {
            entityManager.createNativeQuery(builder.toString()).executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
